export interface Trending {
  page: number;
  results: Result[];
  total_pages: number;
  total_results: number;
}

export interface Result {
  vote_average: number;
  overview: string;
  release_date?: Date;
  adult?: boolean;
  backdrop_path: string;
  vote_count: number;
  genre_ids: number[];
  title?: string;
  original_language: OriginalLanguage;
  original_title?: string;
  poster_path: string;
  id: number;
  video?: boolean;
  popularity: number;
  media_type: MediaType;
  name?: string;
  original_name?: string;
  origin_country?: string[];
  first_air_date?: Date;
}

export enum MediaType {
  Movie = 'movie',
  Tv = 'tv',
}

export enum OriginalLanguage {
  En = 'en',
  Ja = 'ja',
}
