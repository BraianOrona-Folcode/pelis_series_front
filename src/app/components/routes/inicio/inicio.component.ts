import { Component, Input, OnInit } from '@angular/core';
import { MoviesServiceService } from 'src/app/services/movies/movies-service.service';
import { movieSeries } from 'src/interfaces/MovieSeries';
import { Trending } from 'src/interfaces/Trending';
import { Result } from '../../../../interfaces/MoviesPopular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [MoviesServiceService],
})
export class InicioComponent implements OnInit {
  // @Input('inputSearch') inputSearch: string;
  movies_series: movieSeries[] = [];

  movies_seriesAux: movieSeries[] = [];
  movies_seriesToSearch: movieSeries[] = [];
  toSearch: string = '';

  userId: string = '';

  search(s: any) {
    this.toSearch = s.toUpperCase();

    this.movies_seriesToSearch = [];
    for (let search of this.movies_series) {
      if (
        search.title?.toUpperCase().includes(this.toSearch) ||
        search.name?.toUpperCase().includes(this.toSearch)
      ) {
        this.movies_seriesToSearch.push(search);
      }
    }

    if (s !== '') {
      this.movies_seriesAux = this.movies_seriesToSearch;
    } else {
      this.movies_seriesAux = this.movies_series;
    }
  }

  filtro(tipo: string) {
    if (tipo == 'todos') return (this.movies_seriesAux = this.movies_series);

    if (tipo == 'peliculas') {
      this.getMovies();
      // this.movies_seriesAux = this.movies_series.filter(
      //   (e) => e.media_type == 'movie'
      // );
    }

    if (tipo == 'series') {
      this.getSeries();
      // this.movies_seriesAux = this.movies_series.filter(
      //   (e) => e.media_type == 'tv'
      // );
    }
    return;
  }

  constructor(private _movieService: MoviesServiceService) {}

  ngOnInit(): void {
    this.filtro('todos');
    this.getTrending();
    this.userId = JSON.parse(localStorage.getItem('usuario')!).uid;
    console.log(this.userId);
  }

  getTrending() {
    this._movieService.getTrending().subscribe({
      next: (data: any) => {
        console.log(data.results);
        // this.movies_series = data.results;
        this.movies_series = data.results;
        this.movies_seriesAux = this.movies_series;
        console.log(this.movies_series);
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  getMovies() {
    this._movieService.getMovies().subscribe({
      next: (data: any) => {
        this.movies_seriesAux = data.results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  getSeries() {
    this._movieService.getSeries().subscribe({
      next: (data: any) => {
        this.movies_seriesAux = data.results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  getLength() {
    return this.movies_seriesAux?.length || 0;
  }

  addItem(card: movieSeries, type: string) {
    this._movieService
      .addItem(this.userId, card, type)
      .then(() => {
        console.log('res');
      })
      .catch(() => {
        console.log('error');
      });
  }
}
