import { Component, OnInit } from '@angular/core';
import { MoviesServiceService } from 'src/app/services/movies/movies-service.service';
import { movieSeries } from 'src/interfaces/MovieSeries';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css'],
  providers: [MoviesServiceService],
})
export class SeriesComponent implements OnInit {
  movies_series: movieSeries[] = [];

  movies_seriesAux: movieSeries[] = [];
  movies_seriesToSearch: movieSeries[] = [];

  toSearch: string = '';

  search(s: any) {
    this.toSearch = s.toUpperCase();

    this.movies_seriesToSearch = [];
    for (let search of this.movies_series) {
      if (
        search.title?.toUpperCase().includes(this.toSearch) ||
        search.name?.toUpperCase().includes(this.toSearch)
      ) {
        this.movies_seriesToSearch.push(search);
      }
    }

    if (s !== '') {
      this.movies_seriesAux = this.movies_seriesToSearch;
    } else {
      this.movies_seriesAux = this.movies_series;
    }
  }

  constructor(private _movieService: MoviesServiceService) {}

  ngOnInit(): void {
    this.getSeries();
  }

  getSeries() {
    this._movieService.getSeries().subscribe({
      next: (data: any) => {
        console.log(data.results);
        // this.movies_series = data.results;
        this.movies_series = data.results;
        this.movies_seriesAux = this.movies_series;
        console.log(this.movies_series);
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  getLength() {
    return this.movies_seriesAux?.length || 0;
  }
}
