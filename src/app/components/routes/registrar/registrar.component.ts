import { Component, OnInit } from '@angular/core';
import { MoviesServiceService } from '../../../services/movies/movies-service.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css'],
  providers: [MoviesServiceService],
})
export class RegistrarComponent implements OnInit {
  constructor(private _movieService: MoviesServiceService) {}

  ngOnInit(): void {}

  registrar() {
    // this._movieService.addUser(userId);
  }
}
