import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase/firebase.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
// import { FirebaseService } from 'src/app/services/firebase/firebase.service';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css'],
  // providers: [FirebaseService],
})
export class IngresarComponent implements OnInit {
  login: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.email]],
    password: [, [Validators.required, Validators.minLength(6)]],
  });

  constructor(
    private fb: FormBuilder,
    private fbService: FirebaseService,
    private router: Router,
    private afauth: AngularFireAuth
  ) {}

  ngOnInit(): void {}

  public ingresar() {
    if (this.login.invalid) {
      return;
    }
    const { email, password } = this.login.controls;
    this.fbService.login(email.value, password.value).then((response) => {
      if (!response) return;
      console.log('Se Registro: ', response);
      this.router.navigate(['/inicio']);
    });
  }

  public ingresarConGoogle() {
    console.log('Ingresar');

    this.fbService.loginWithGoogle().then((response) => {
      if (!response) {
        return;
      }
      localStorage.setItem('usuario', JSON.stringify(response.user));
      console.log('Se Registro: ', response);
      this.router.navigate(['/inicio']);
    });
  }

  getUserLogged() {
    return this.afauth.authState;
  }

  logout() {
    this.afauth.signOut();
  }
}
