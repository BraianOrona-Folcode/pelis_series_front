import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { movieSeries } from 'src/interfaces/MovieSeries';
import { MoviesServiceService } from '../../../services/movies/movies-service.service';

@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.css'],
})
export class MyListComponent implements OnInit {
  type: string = '';
  userId: string = '';

  constructor(
    private _route: ActivatedRoute,
    private movieServiceService: MoviesServiceService
  ) {
    this.type = _route.snapshot.paramMap.get('type')!;
    console.log(this.type);
  }

  ngOnInit(): void {
    this.userId = JSON.parse(localStorage.getItem('usuario')!).uid;
    if (this.type == 'tv') {
      this.getList(this.type);
    }
    if (this.type == 'movie') {
      this.getList(this.type);
    }
  }

  getList(type: string) {
    this.movieServiceService.getList(this.userId, type).subscribe(
      (response) => {
        this.movies_series = [];
        console.log('saved: ', response);
        response.forEach((element: any) => {
          console.log(element.payload.doc.id);
          console.log(element.payload.doc.data());
          this.movies_series.push({
            idCard: element.payload.doc.id,
            ...element.payload.doc.data(),
          });
        });
        console.log(this.movies_series);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  deleteItem(idCard: string) {
    this.movieServiceService
      .deleteItem(this.userId, idCard, this.type)
      .then(() => {
        console.log('Elemento elminiado');
      })
      .catch(() => {
        console.log('error');
      });
  }

  getLength() {
    return this.movies_series?.length || 0;
  }

  movies_series: movieSeries[] = [];

  movies_seriesAux: movieSeries[] = [];
  movies_seriesToSearch: movieSeries[] = [];

  toSearch: string = '';

  search(s: any) {
    this.toSearch = s.toUpperCase();

    this.movies_seriesToSearch = [];
    for (let search of this.movies_series) {
      if (
        search.title?.toUpperCase().includes(this.toSearch) ||
        search.name?.toUpperCase().includes(this.toSearch)
      ) {
        this.movies_seriesToSearch.push(search);
      }
    }

    if (s !== '') {
      this.movies_seriesAux = this.movies_seriesToSearch;
    } else {
      this.movies_seriesAux = this.movies_series;
    }
  }
}
