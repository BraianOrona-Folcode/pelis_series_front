import { Component, OnInit } from '@angular/core';
import { MoviesServiceService } from '../../../services/movies/movies-service.service';
import { Observable } from 'rxjs';
import { movieSeries } from 'src/interfaces/MovieSeries';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  // lenghtPelicula: Observable<any> | undefined;
  lenghtPelicula: number = 0;
  lenghtSerie: number = 0;
  movies_series: movieSeries[] = [];
  userId: string = '';

  constructor(private _movieService: MoviesServiceService) {}

  ngOnInit(): void {
    this.userId = JSON.parse(localStorage.getItem('usuario')!).uid;
    this.getLengthPeliculas();
    this.getLengthSeries();
  }

  getLengthPeliculas() {
    this._movieService.getLengthPelicula(this.userId).subscribe((response) => {
      this.movies_series = [];
      response.forEach((element: any) => {
        this.movies_series.push({
          globalId: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
      this.lenghtPelicula = this.movies_series.length;
    });
    console.log(`Pelis: ${this.lenghtPelicula}`);
  }

  getLengthSeries() {
    this._movieService.getLengthSeries(this.userId).subscribe((response) => {
      this.movies_series = [];
      response.forEach((element: any) => {
        this.movies_series.push({
          globalId: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
      this.lenghtSerie = this.movies_series.length;
    });
    console.log(`Series: ${this.lenghtSerie}`);
  }

  getLength(type: string) {}
}
