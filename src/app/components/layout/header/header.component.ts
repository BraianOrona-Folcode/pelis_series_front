import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FirebaseService } from '../../../services/firebase/firebase.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(private afauth: AngularFireAuth) {}

  ngOnInit(): void {}

  public userIsLogged: Observable<any> = this.afauth.user;

  logout() {
    this.afauth.signOut();
  }
}
