import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { PeliculasComponent } from './components/routes/peliculas/peliculas.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';
import { RegistrarComponent } from './components/routes/registrar/registrar.component';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { MyListComponent } from './components/routes/my-list/my-list.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent,
  },
  {
    path: 'peliculas',
    component: PeliculasComponent,
  },
  {
    path: 'series',
    component: SeriesComponent,
  },
  {
    path: 'ingresar',
    component: IngresarComponent,
  },
  {
    path: 'registrar',
    component: RegistrarComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'mylist/:type',
    component: MyListComponent,
  },
  {
    path: '**',
    redirectTo: 'inicio',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
