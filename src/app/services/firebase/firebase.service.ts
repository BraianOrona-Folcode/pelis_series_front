import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { GoogleAuthProvider } from 'firebase/auth';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  constructor(private afauth: AngularFireAuth) {}

  async login(email: string, password: string) {
    try {
      return await this.afauth.signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.log('error en login: ', error);
      return null;
    }
  }

  async loginWithGoogle() {
    try {
      return await this.afauth.signInWithPopup(new GoogleAuthProvider());
    } catch (error) {
      console.log('error en login con google: ', error);
      return null;
    }
  }
}
