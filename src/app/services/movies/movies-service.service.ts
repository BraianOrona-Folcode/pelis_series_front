import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Trending } from 'src/interfaces/Trending';
import { MoviesPopular } from '../../../interfaces/MoviesPopular';
import { SeriesPopular } from 'src/interfaces/SeriesPopular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import * as firebase from 'firebase/compat';
import { movieSeries, MovieSerieUser } from '../../../interfaces/MovieSeries';

@Injectable({
  providedIn: 'root',
})
export class MoviesServiceService {
  private baseURL: string = 'https://api.themoviedb.org/3';

  private apiKey: string = '1a9bb7718eefa91de2cf5bedd6827b95';

  constructor(private _http: HttpClient, private firestore: AngularFirestore) {}

  getTrending(): Observable<Trending[]> {
    return this._http.get<Trending[]>(
      `${this.baseURL}/trending/all/week?api_key=${this.apiKey}`
    );
  }

  getMovies(): Observable<MoviesPopular[]> {
    return this._http.get<MoviesPopular[]>(
      `${this.baseURL}/movie/popular?api_key=${this.apiKey}`
    );
  }

  getSeries(): Observable<SeriesPopular[]> {
    return this._http.get<SeriesPopular[]>(
      `${this.baseURL}/tv/popular?api_key=${this.apiKey}`
    );
  }

  public addMovieSerie(value: movieSeries, type: string) {
    if (type === 'tv') {
      return this.firestore.collection('tv').add(value);
    } else {
      return this.firestore.collection('movie').add(value);
    }
  }

  addUser(userId: MovieSerieUser): Promise<any> {
    return this.firestore.collection('usuarios').add(userId);
  }

  addItem(userId: string, item: movieSeries, collection: string): Promise<any> {
    return this.firestore
      .collection('usuarios')
      .doc(`${userId}`)
      .collection(collection)
      .add(item);
  }

  getList(userId: string, type: string): Observable<any> {
    return this.firestore
      .collection('usuarios')
      .doc(`${userId}`)
      .collection(type)
      .snapshotChanges();
  }

  deleteItem(idUser: string, id: string, type: string): Promise<any> {
    return this.firestore
      .collection(`usuarios/${idUser}/${type}`)
      .doc(id)
      .delete();
  }

  getLengthPelicula(userId: string) {
    return this.firestore
      .collection('usuarios')
      .doc(`${userId}`)
      .collection('movie')
      .snapshotChanges();
  }

  getLengthSeries(userId: string) {
    return this.firestore
      .collection('usuarios')
      .doc(`${userId}`)
      .collection('tv')
      .snapshotChanges();
  }
}
