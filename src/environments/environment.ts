// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: 'AIzaSyDr6aP4aR87LGFPCVuT9dYi9tn00bP_9AA',
    authDomain: 'pelisup-braianorona.firebaseapp.com',
    projectId: 'pelisup-braianorona',
    storageBucket: 'pelisup-braianorona.appspot.com',
    messagingSenderId: '92355009474',
    appId: '1:92355009474:web:2c11c14ebba37446ba545b',
    measurementId: 'G-MWERWCMF9J',
  },
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
